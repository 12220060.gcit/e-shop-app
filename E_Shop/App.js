import "react-native-gesture-handler";
import * as React from "react";
import { StyleSheet, View, LogBox } from "react-native";
import { NavigationContainer } from "@react-navigation/native";

//Redux
import { Provider } from "react-redux";
import store from "./Redux/store";

//Navigators
import Main from "./Navigators/Main";
import Header from "./Shared/Header";
//Screens
import ProductContainer from "./Screens/Products/ProductContainer";
LogBox.ignoreAllLogs(true);
export default function App() {
  return (
    <Provider store={store}>
    <NavigationContainer>
      <Header />
      <Main />
    </NavigationContainer>
    </Provider>
  );
}
