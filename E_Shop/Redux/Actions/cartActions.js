import { ADD_TO_CART, REMOVE_FROM_CART, CLEAR_CART } from '../constants';

// Action creator to add an item to the cart
export const addToCart = (payload) => {
  return {
    type: ADD_TO_CART,
    payload
  };
};

// Action creator to remove an item from the cart
export const removeFromCart = (payload) => {
  return {
    type: REMOVE_FROM_CART,
    payload
  };
};

// Action creator to clear the cart
export const clearCart = () => {
  return {
    type: CLEAR_CART
  };
};
